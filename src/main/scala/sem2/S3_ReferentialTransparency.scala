package sem2

import cats.effect.IO

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

object S3_ReferentialTransparency {
  def printHello(): Unit = println("Hello")

  val printHelloFuture: Future[Unit] = Future(printHello())
  val printHelloIO: IO[Unit] = IO(printHello())

  def main(args: Array[String]): Unit = {
    println("-------------- future1")
    val future1 = for {
      _ <- Future(printHello())
      _ <- Future(printHello())
    } yield ()

    Await.result(future1, Duration.Inf)

    println("-------------- future2")

    val future2 = for {
      _ <- printHelloFuture
      _ <- printHelloFuture
    } yield ()

    Await.result(future2, Duration.Inf)

    println("---------- io1")
    val io1 = for {
      _ <- IO(printHello())
      _ <- IO(printHello())
    } yield ()

    io1.unsafeRunSync()

    println("---------- io2")
    val io2 = for {
      _ <- printHelloIO
      _ <- printHelloIO
    } yield ()

    io2.unsafeRunSync()
  }
}
