import cats.effect.IO
import cats.syntax.traverse._

// IO.pure - pure value + side effect | call by value
IO.pure(println("Hello"))

// IO.delay - pure value + side effect | call by name
val printHello = IO.delay(println("Hello"))

printHello.unsafeRunSync()

// IO.raiseError


val io = for {
  _ <- IO.raiseError(new Exception("Message 1"))
  _ <- IO.raiseError(new Exception("Message 2"))
  _ <- printHello
} yield ()

// IO.handleErrorWith

val io2 = io.handleErrorWith {
  err: Throwable => IO(println(err.getMessage))
}

io2.unsafeRunSync()

// IO.sequence

val listIO = List(IO(1), IO(2)).sequence
listIO.unsafeRunSync()

// IO.traverse

val printList = List(1, 2).traverse(i => IO(println(i)))
printList.unsafeRunSync()
