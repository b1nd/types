package sem2.tagless

import cats.{Applicative, ApplicativeThrow, MonadThrow}
import cats.syntax.flatMap._
import cats.syntax.functor._
import domain.{User, Username}

trait UserService[F[_]] {
  def getUser(username: Username): F[User]
}

class UserServiceImpl[F[_]: MonadThrow](implicit userDao: UserDao[F])
    extends UserService[F] {

  override def getUser(username: Username): F[User] =
    for {
      maybeUser <- userDao.getUser(username)
      user      <- maybeUser match {
                     case Some(user) => Applicative[F].pure(user)
                     case None       =>
                       ApplicativeThrow[F].raiseError(
                         new Exception(s"User $username not found!")
                       )
                   }
    } yield user
}
