package sem2.tagless

object domain {
  // google: newtype scala
  case class Username(value: String)

  case class User(username: Username, name: String, age: Int)
}
