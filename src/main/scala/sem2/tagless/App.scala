package sem2.tagless

import cats.effect.{ExitCode, IO, IOApp}
import cats.syntax.applicativeError._
import sem2.tagless.domain.{User, Username}

object App extends IOApp {
  type F[+A] = IO[A]

  override def run(args: List[String]): F[ExitCode] = {
    implicit val dao: UserDao[F] = new UserDaoImpl[F]
    val service: UserService[F]  = new UserServiceImpl[F]()

    val program = for {
      _    <- dao.saveUser(User(Username("user123"), "Alexey", 18))
      user <- service.getUser(Username("user123"))
      _    <- IO(println(user))
    } yield ()

    program.map(_ => ExitCode.Success)
      .handleError(_ => ExitCode.Error)
  }
}
