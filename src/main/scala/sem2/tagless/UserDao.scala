package sem2.tagless

import cats.effect.Sync
import domain.{User, Username}

import scala.collection.mutable

trait UserDao[F[_]] {
  def saveUser(user: User): F[Unit]
  def getUser(username: Username): F[Option[User]]
}

class UserDaoImpl[F[_]](implicit F: Sync[F]) extends UserDao[F] {
  private val map: mutable.Map[Username, User] = mutable.Map.empty

  override def saveUser(user: User): F[Unit] =
    F.delay(map.addOne(user.username -> user))

  override def getUser(username: Username): F[Option[User]] =
    F.delay(map.get(username))
}
