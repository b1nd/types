def getX: Int = {
  println("getX call |")
  1
}

// No X usage
def callByValue(x: Int): Int   = 0
def callByName(x: => Int): Int = 0

callByValue(getX)
callByName(getX)

// Single X usage
def callByValue(x: Int): Int   = x
def callByName(x: => Int): Int = x

callByValue(getX)
callByName(getX)

// Multiple X usage
def callByValue(x: Int): Int   = x * x
def callByName(x: => Int): Int = x * x

callByValue(getX)
callByName(getX)

lazy val x1 = getX
x1
x1