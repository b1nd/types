import cats.effect.{IO, Resource}
import cats.syntax.flatMap._

import java.io.{BufferedWriter, FileWriter, Writer}

// file

def newFileIO(out: String): IO[BufferedWriter] = IO {
  new BufferedWriter(new FileWriter(s"D:/repos/types/$out.txt"))
}

def closeWriter(writer: Writer, i: Int) = for {
  _ <- IO(writer.close())
  _ <- IO(println(s"Closed $i"))
} yield ()

def printHello(writer: Writer): IO[Unit] = for {
  _ <- IO(writer.write("hello1"))
  _ <- IO(writer.write("hello1"))
} yield ()

val resource1 = Resource.make(newFileIO("out1"))(closeWriter(_, 1))
val resource2 = Resource.make(newFileIO("out2"))(closeWriter(_, 2))

// compose resources

val resource = for {
  writer1 <- resource1
  writer2 <- resource2
} yield (writer1, writer2)

val io: IO[Unit] = resource.use { case (writer1, writer2) =>
  printHello(writer1) >> printHello(writer2)
}

io.unsafeRunSync()
io.unsafeRunSync()
