trait Container {
  type A
  val value: A
  def getValue: A = value
}

val intContainer = new Container {
  override type A = Int
  override val value: A = 1
}
val stringContainer = new Container {
  override type A = String
  override val value: A = "Hello"
}

intContainer.getValue
stringContainer.getValue

// type constraint
abstract class Animal {
  def name: String
}
case class Cat(name: String) extends Animal
case class Dog(name: String) extends Animal

trait AnimalContainer extends Container {
  type A <: Animal
}

val catContainer = new AnimalContainer {
  override type A = Cat
  override val value: A = Cat("cat")
}

val longContainer = new AnimalContainer {
  override type A = Long // error
  override val value: A = 1L
}

catContainer.getValue
longContainer.getValue
