import cats.effect.IO

import scala.collection.mutable
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

// class extends function
class Handler extends (String => String) {
  override def apply(v1: String): String = v1.toUpperCase
}

val h = new Handler
h("hello")

val set = Set(1, 2, 3)

// def, val, lazy val
trait Printer {
  def print: String
}

case class Cat(name: String) extends Printer {
  override val print: String = {
    println("--- Cat")
    s"Cat's name: $name"
  }
}

case class ImmutableMap(map: Map[String, String]) extends Printer {
  override lazy val print: String = {
    println("-- ImmutableMap")
    "ImmutableMap: " + map.toString()
  }
}

case class MutableMap(map: mutable.Map[String, String]) extends Printer {
  override def print: String = {
    println("-- MutableMap")
    "MutableMap: " + map.toString()
  }
}

val cat = Cat("Cat")
cat.print
cat.print

val immutable = ImmutableMap(Map("K1" -> "V1", "K2" -> "V2"))
immutable.print
immutable.print

val map = mutable.Map("K1" -> "V1", "K2" -> "V2")
val mutableMap = MutableMap(map)
mutableMap.print
map.addOne("K3" -> "V3")
mutableMap.print

def add(x: Int, y: Int): Int = x + y

Future(println(1))

IO(println(1))
IO(println(1))
