// Is type extractor
object HasThreeWords {
  def unapply(arg: String): Boolean = arg.split("\\s+").length == 3
}

"a b c" match {
  case HasThreeWords() => println("YES")
  case _ => println("NO")
}

// Maybe type extractor
object FIO {
  def unapply(fio: String): Option[(String, String, String)] =
    fio.split("\\s+") match {
      case Array(firstName, lastName, coName) =>
        Some(firstName.toUpperCase, lastName.toUpperCase, coName.toUpperCase)
      case _                                  => None
    }
}

"Foo Bar Baz" match {
  case FIO(f, i, o) =>
    println(s"Фамилия: $f")
    println(s"Имя: $i")
    println(s"Отчество: $o")
  case s => println(s"Not fio: $s")
}

// Maybe sequence type extractor
object Words {
  def unapplySeq(words: String): Option[Seq[String]] = {
    val split = words.split("\\s+").toSeq
    if (split.nonEmpty) Some(split)
    else None
  }
}

"a b c d" match {
  case Words(a, b, c, d) => println("4")
  case Words(a, b) =>
}

// example: regexp extractor
val FindNumber = """(.*?)(\d+)(.*?)""".r

"one123two" match {
  case FindNumber(_, num, _) => println(num)
  case _ => println("No number")
}

// pair, case class, etc

case class Account(amount: Double)
val account = Account(100)

val Account(amount) = account
amount + 200

val tuple = (1, "one")

val (num, string) = tuple

tuple match {
  case (1, _) => println("1")
  case (1, s) if s.length == 3 => println("2")
  case (_, s) => println(s)
}

sealed class A
object B extends A
object C extends A

def adt(a: A) = a match {
  case B | C => ???
  case _ => ???
}
