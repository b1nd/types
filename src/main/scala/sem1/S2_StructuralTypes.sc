import cats.effect.{IO, Resource}

import scala.language.reflectiveCalls

// interface bound
def doAndCloseBound[T <: AutoCloseable](t: T)(action: T => Unit): Unit = {
  action(t)
  t.close()
}

// uses reflection, so be performance-aware!
def doAndCloseStructural[T <: { def close(): Unit }](t: T)(action: T => Unit): Unit= {
  action(t)
  t.close()
}

class A /*extends AutoCloseable*/ {
  def close(): Unit = println("Close")
  def print(s: String): Unit = println(s"Print: $s")
}

val a = new A

def printAll(a: A): Unit = {
  a.print("1")
  a.print("2")
  a.print("3")
}

// doAndCloseBound(a)(printAll) // error if a is not AutoCloseable
doAndCloseStructural(a)(printAll)

// regular Scala way
// Resource.make(IO.pure(new A))(t => IO(t.close()))