// * -> *
trait Container[M[_]] {
  def put[A](x: A): M[A]
  def get[A](m: M[A]): A
}

def tupleize[M[_]: Container, A, B](a: M[A], b: M[B]): M[(A, B)] = {
  val c = implicitly[Container[M]]
  c.put(c.get(a), c.get(b))
}

implicit val containerOption: Container[Option] = new Container[Option] {
  override def put[A](x: A) = Option(x)
  override def get[A](m: Option[A]) = m.get
}
implicit val containerList: Container[List] = new Container[List] {
  override def put[A](x: A) = List(x)
  override def get[A](m: List[A]) = m.head
}

tupleize(Option(1), Option(2))
tupleize(Option("one"), Option("two"))
tupleize(List(1), List(2))
tupleize(List("one"), List("two"))
