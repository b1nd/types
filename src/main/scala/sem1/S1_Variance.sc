import cats.Show
import cats.Show.ContravariantShow

class Baz[A]  // An invariant class
class Foo[+A] // A covariant class
class Bar[-A] // A contravariant class

abstract class Animal {
  def name: String
}
case class Cat(name: String) extends Animal
case class Dog(name: String) extends Animal

// Array (invariant)
def printAnimalsArray(animals: Array[Animal]): Unit =
  animals.foreach(println)

def printAnimalsArray[T >: Cat <: Animal](animals: Array[T]): Unit =
  animals.foreach(println)

val cat = Array(Cat("1"), Cat("2"))

//printAnimalsArray(cat)

// List (covariant)
def printAnimalsList(animals: List[Animal]): Unit =
  animals.foreach(println)

val catList = List(Cat("1"), Cat("2"))
printAnimalsList(catList)

// Show (contravariant)
def printCat(show: ContravariantShow[Cat], cat: Cat): Unit =
  println(show.show(cat))

implicit val catShow: Show[Cat] = (t: Cat) => s"Cat's name: ${t.name}"
implicit val animalShow: Show[Animal] = (t: Animal) => s"Animal's name: ${t.name}"

printCat(catShow, Cat("Cat 1"))
printCat(animalShow, Cat("Cat 2"))
