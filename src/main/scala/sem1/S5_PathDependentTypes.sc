// problem
case class Account(amount: Double, owner: String)

case class Owner(name: String) {
  def createAccount(amount: Double) = Account(amount, name)

  def combine(a1: Account, a2: Account): Account = {
    require(a1.owner == a2.owner) // assert runtime failure
    Account(a1.amount + a2.amount, a1.owner)
  }
}

val alice        = Owner("Alice")
val aliceAccount = alice.createAccount(100)

val bob        = Owner("Bob")
val bobAccount = bob.createAccount(1000)

alice.combine(aliceAccount, bobAccount) // runtime error

// Path Dependent
case class Owner(name: String) {
  case class Account(amount: Double, owner: String)

  def createAccount(amount: Double) = Account(amount, name)

  def combine(a1: Account, a2: Account): Account =
    Account(a1.amount + a2.amount, a1.owner)
}

val alice1       = Owner("Alice")
val aliceAccount = alice1.createAccount(100)

val bob1       = Owner("Bob")
val bobAccount = bob1.createAccount(1000)

alice1.combine(aliceAccount, bobAccount) // compile error

// Cats combine
import cats.kernel.Semigroup
import cats.syntax.semigroup._

case class Owner(name: String) {
  case class Account(amount: Double, owner: String)
  object Account {
    implicit val accountSemigroup: Semigroup[Account] =
      (x: Account, y: Account) => x.copy(amount = x.amount + y.amount)
  }

  def createAccount(amount: Double) = Account(amount, name)
}

val alice1       = Owner("Alice 1")
val aliceAccount1 = alice1.createAccount(100)
val aliceAccount2 = alice1.createAccount(1000)
val aliceAccount3 = alice1.createAccount(10000)

aliceAccount1 |+| aliceAccount2 |+| aliceAccount3
