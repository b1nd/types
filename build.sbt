name := "types"

version := "0.1"

scalaVersion := "2.13.7"

libraryDependencies ++= Seq(
  "org.typelevel" %% "cats-core"   % "2.6.1",
  "org.typelevel" %% "cats-effect" % "2.5.4"
)
